use rocket::fs::FileServer;
use rocket::Build;
use rocket::Rocket;
use rocket_dyn_templates::Template;

pub fn build_rocket() -> Rocket<Build> {
    let mut build = rocket::build()
        .mount("/public", FileServer::from("static/"))
        .attach(Template::fairing());
    build = crate::controllers::register_catchers(build);
    build = crate::controllers::mount_routes(build);
    build
}