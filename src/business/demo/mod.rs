
// MODEL

pub struct Demo {
    pub name: String,
    pub age: i32,
}

impl Demo {
    pub fn new(name: String, age: i32) -> Demo {
        Demo {
            name: name,
            age: age,
        }
    }
}

// TRAITS

pub trait DemoBusiness {
    fn find_by_name(&self, name: &str) -> Option<Demo>;
}

pub trait DemoRepository {
    fn find_by_name(&self, name: &str) -> Option<Demo>;
}

// SERVICE IMPL

struct DemoBusinessImpl {
    repository: Box<dyn DemoRepository>
}

impl DemoBusinessImpl {

    pub fn new(repository: Box<dyn DemoRepository>) -> DemoBusinessImpl {
        DemoBusinessImpl{
            repository: repository
        }
    }

}

impl DemoBusiness for DemoBusinessImpl {
    fn find_by_name(&self, name: &str) -> Option<Demo> {
        self.repository.find_by_name(name)
    }   
}
