use rocket::response::Redirect;
use rocket::{Build, Rocket};
use rocket_dyn_templates::Template;

pub fn mount_routes(build: Rocket<Build>) -> Rocket<Build> {
    build.mount("/", routes![index, hello])
}

#[get("/")]
pub fn index() -> Redirect {
    Redirect::to(uri!("/", hello(name = "Juanito")))
}

#[get("/hello/<name>")]
pub fn hello(name: &str) -> Template {
    let mut context = super::default_context();
    context.insert("name", name);
    context.insert("items", &vec!["One", "Two", "Three"]);
    return Template::render("index", context.into_json());
}
