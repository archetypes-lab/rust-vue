pub mod demo;

use rocket_dyn_templates::{tera::Context, Template};
use rocket::{Build, Rocket, Request};

pub fn mount_routes(mut build: Rocket<Build>) -> Rocket<Build> {
    build = demo::mount_routes(build);
    build
}

pub fn register_catchers(build: Rocket<Build>) -> Rocket<Build> {
    build.register("/", catchers![not_found])
}

fn default_context() -> Context {
    let mut context = Context::new();
    context.insert("title", "Hello");
    context.insert("VUE_VERSION", "2.6.11");
    context.insert("PRIMEVUE_VERSION", "2.0.0-beta.1");
    context.insert("PRIMEICONS_VERSION", "3.0.0-rc.1");
    context.insert("JQUERY_VERSION", "3.3.1");
    context.insert("JQUERY_EASING_VERSION", "1.4.1");
    context.insert("JQUERY_VALIDATION_VERSION", "1.19.1");
    context.insert("AXIOS_VERSION", "0.19.2");
    context.insert("LODASH_VERSION", "4.17.15");
    context.insert("MOMENT_VERSION", "2.24.3");
    context.insert("VEE_VALIDATE_VERSION", "3.3.0");
    context
}

#[catch(404)]
pub fn not_found(req: &Request<'_>) -> Template {
    let mut context = Context::new();
    context.insert("uri", req.uri());
    return Template::render("error/404", context.into_json());
}