#[macro_use]
extern crate rocket;

mod business;
mod clients;
mod configs;
mod controllers;
mod framework;

#[rocket::main]
async fn main() {
    let rocket_build = configs::rocket::build_rocket();
    let _result = rocket_build.launch().await;

    // this is reachable only after `Shutdown::notify()` or `Ctrl+C`.
    println!("Rocket: deorbit.");
}
